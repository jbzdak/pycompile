package pl.askesis.pycompile.config

import org.junit.Assert
import org.junit.Test
import pl.askesis.pycompile.BaseSystem
import java.net.URI
import java.net.URL

class ConfigTest{

  private fun verifyTestConfig(parsed: ParsedConfig?){
    Assert.assertNotNull(parsed)
    parsed!!
    Assert.assertEquals(parsed.jobs.size, 1);
    val job = parsed.jobs[0];
    Assert.assertEquals(job.baseSystem, BaseSystem.DEBIAN_JESSIE);
    Assert.assertEquals(job.pythonVersion, "3.5.1");
    Assert.assertEquals(job.compileDependencies.size, 1);

    val dependency = job.compileDependencies[0];

    Assert.assertEquals(dependency.packageName, "python3.4")

    Assert.assertEquals(parsed.pythons.size, 1)

    Assert.assertTrue(parsed.pythons.containsKey("3.5.1"))

    val python = parsed.pythons.get("3.5.1");
    Assert.assertNotNull(python);
    python!!
    Assert.assertEquals(
      python.downloadUrl,
      "https://www.python.org/ftp/python/3.5.1/Python-3.5.1.tar.xz"
    )
    Assert.assertEquals(
      python.hexEncodedSha,
      "c6d57c0c366d9060ab6c0cdf889ebf3d92711d466cc0119c441dbf2746f725c9"
    )
  }

  @Test
  fun testParse(){
    val parsed = ConfigParser.read(
      javaClass.getResourceAsStream("/testConfig.yml")
    )

    verifyTestConfig(parsed)
  }

  @Test
  fun testUrlClasspath(){
    val parsed = ConfigParser.read(
      URI("classpath:///testConfig.yml")
    )
    verifyTestConfig(parsed)
  }
}
