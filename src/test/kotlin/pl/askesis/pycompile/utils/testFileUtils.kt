package pl.askesis.pycompile.utils

import org.apache.commons.io.FileUtils
import org.junit.*
import org.junit.rules.TemporaryFolder
import java.net.URL
import java.nio.file.Files
import javax.xml.bind.DatatypeConverter

open class BaseFileTest {

  val tempDir = Files.createTempDirectory("compilepy");
  val tempFile = tempDir.resolve("test");
  val exampleContents = resourceToString("/testFileForHashing.txt");

  val expectedHash = "e9a8f9bee5f228168ccf51e4f6456bc2e7879bfad419c5f349f632a84c6536a1";
  val expectedBytes = DatatypeConverter.parseHexBinary(expectedHash);

  @After
  open fun tearDown(){
    FileUtils.deleteDirectory(tempDir.toFile());
  }
}

class TestFileHashing: BaseFileTest(){

  @Before
  fun setUp(){
    copyStringToFile(resourceToString("/testFileForHashing.txt"), tempFile);
  }

  @Test
  fun testFileHashing(){
    val actualBytes = calculateFileSha256(tempFile);

    Assert.assertArrayEquals(expectedBytes, actualBytes);
  }

  @Test
  fun testFileHashingOnNotexistentFile(){
    Files.delete(tempFile);
    Assert.assertNull(calculateFileSha256(tempFile));
  }

  @Test
  fun testStringHashing(){
    val inputString = """
16QKaKiKCZMLqLXGPfCnZTA/MY/rCpm2sTfsYRLnMlFIdeg8qEWcJj+YJO8Ny2WO77Bf8Wt30CZj
1VmVr272c6vn+4JSdKiXLLyLkMA6c7BoQnr8ybVKNXQG2P9tFe8BC+LszYfHb+4iBOSMDxJfHi14
NthiwXLaTlzWjT0diWKBKIaFUSKNRH2bWnU7gjI7SdPP7GwDbCpH8sYZ4PK6ojGr+XhG4tX/ZCP9
wyMugce8/sX6+i+5oHeN3+6ITUBVS6hbi+Q/NC5R6zRkduZt3iILbrTIBFcu7o2D7T6az0ikl5XN
0A8UcZmyBFiQe02iMIGg6uo+JgsdElTjhVOdNVccw4fgaf4LNT18AHPTTF++TONMoELMjxp+AlfP
m81z2x0kFLHCrWyuZliIuMOo4dAsoTubN2gWaCFsJOLz186jXYu/+lAWOeL4aC9/vy8qCotl1kFx
l+/iKOQzIDobDXx2tCersygtlKeuiJIoCw/AA32wM7XFhP1JjZT7UhnEy0WJBC6SZcbHSMf5uuH/
IXSRJUg6K2vqXF3/K+STQowdRYyNgKMVWof1AI5wUTnSnnviEQXDpvUNCKBLlX+tys5jeMYTNU9E
5BwQQaAlSQguz74RlNV+Mp8jRU7G0ueSG9xP0K/gz0rHR2ryBYalkNpTHY+mgRFUWwlBFdxCgBus
m5KylqXgoVwksPJhTN755pT1ok95BuH6dy9UPJII9a4xT8Nlun5imPSMuos3Wbs5mDYkpYykW9TN
gmsXy3RRpnvQREFVhe7UxbdHbm/7HC2mRyvaocoZDTu98NtqThOmqBASp9nM3ZF0m8skIQC8h9GK
YzTWyOgFlU3W7RTGsrzMo7SASz2aj3OX1gj2EMCMVhUpEClWD4ZimZTJEJe1J1bktwltE9sKmfZE
xJUDi6Zl7tFXyr7Qb2k/0t8LqET0d3NNDqsDhoLLlIs1YR9kSuE+Swdcv+p9s7yABfzvxmVQkdPV
048rRS/U0k70aFv2uimNpdrpt7DEx5naoc/loY5dXN8vgp3psI6ElFe+fZfGiqtlpDQ7PssdENgu
m9tTMdgD9rqTZ52gA0i2kqUXEyQEP7CPo+PZd6T5k2PBIOhtYyTDWLPlCyWJtCJB+7ySrgIR3l+E
aIucs9P1OwygyDzCw47ACR57ZcDCzbVFj4UKaZ2jjOxCgCC2fn70Z+M31pqI/euXmT4UZPHL+O3b
TY0o6XvHa+P4BRhmzHmTCcnfjuwy1oPIyldBma42z0A9aL+h0bGIL1NTlnVtTzUiG4cR90cLo0X/
FJy6of8RpS2hzbof2dnbclvPlQYKZhxLLtf/06c9t9f7hY7qcjeKtpxFRE136I8U2RBPDOCzNQ==
""".trim() + "\n"

    Assert.assertArrayEquals(
      expectedBytes,
      calculateStringSha256(inputString)
    )
  }
}

class TestFileCopying: BaseFileTest(){


  /**
   * Technically it does not test anything --- will fail if we won't
   * detect change in lastModified field.
   */
  @Test
  fun testSanity(){
    copyStringToFile(exampleContents, tempFile);
    val initialModificationTime = tempFile.toFile().lastModified();
    // FS guarantees one second resolution.
    Thread.sleep(1000);
    copyStringToFile(exampleContents, tempFile);
    Assert.assertTrue(
      initialModificationTime < tempFile.toFile().lastModified()
    )
  }

  /**
   * Test that when file is not changed we will not change modification time.
   */
  @Test
  fun testNotOverrideIfUnchanged(){
    copyStringToFile(exampleContents, tempFile);
    val initialModificationTime = tempFile.toFile().lastModified();
    // FS guarantees one second resolution.
    Thread.sleep(1000);
    copyStringToFileIfChanged(exampleContents, tempFile);
    Assert.assertTrue(
      initialModificationTime == tempFile.toFile().lastModified()
    )
  }
}

class TestFileDownload: BaseFileTest(){

  val fileUrl = "https://storage.googleapis.com/pycompile/testFileForHashing.txt";

  @Test
  fun testFileDownload(){
    downloadFile(URL(fileUrl), tempFile, expectedBytes);
    Assert.assertTrue(Files.exists(tempFile));
    Assert.assertArrayEquals(
      calculateFileSha256(tempFile),
      expectedBytes
    )
  }

  @Test
  fun testFileDownloadIfFileMissing(){
    downloadFileIfMissing(URL(fileUrl), tempFile, expectedBytes);
    Assert.assertTrue(Files.exists(tempFile));
    Assert.assertArrayEquals(
      calculateFileSha256(tempFile),
      expectedBytes
    )
  }


  @Test
  fun testNotOverrideIfUnchanged(){
    downloadFileIfMissing(URL(fileUrl), tempFile, expectedBytes);
    val initialModificationTime = tempFile.toFile().lastModified();
    // FS guarantees one second resolution.
    Thread.sleep(1000);
    downloadFileIfMissing(URL(fileUrl), tempFile, expectedBytes);
    Assert.assertTrue(
      initialModificationTime == tempFile.toFile().lastModified()
    )
  }

  @Test(expected = InvalidHashException::class)
  fun testFileDownloadNegative(){
    downloadFile(URL(fileUrl), tempFile, ByteArray(0));
  }

}