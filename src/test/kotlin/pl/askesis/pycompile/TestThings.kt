package pl.askesis.pycompile

/**
 * Created by jb on 6/11/16.
 */

import org.apache.commons.io.FileUtils
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.nio.file.Files
import java.nio.file.Paths

class TestJobContext{

  @Test fun testFileExists(){
    val context = JobContext.mockContext();
    Assert.assertTrue(context.tempDir.toFile().exists());
    context.close();
  }

  @Test fun testTempDirCleared(){
    val context = JobContext.mockContext();
    context.tempDir;
    context.close();
    Assert.assertFalse(context.tempDir.toFile().exists())
  }
}


class TestDockerExecutor{
  var job = PythonCompileJobDefinition(
    BaseSystem.DEBIAN_JESSIE,
    "3.5.1",
    compileDependencies = arrayListOf(
      DebDependency("python3.4"),
      DebDependency("python3-numpy")
    )
  );
  val context = JobContext.testContext()
  init {
    context.tempDir = Paths.get("/tmp/test-pycompile");
    Files.createDirectories(context.tempDir);
    context.prepareContext();
  }
  var executor = DockerExecutor(job, context);
  var expectedTemplate = """
FROM debian:jessie
COPY sources.list /etc/apt/sources.list

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get --assume-yes build-dep --no-install-recommends  python3.4  python3-numpy
    """.trim()

  @After
  fun cleanUp(){
    println(context.tempDir)
  }

  @Test
  fun testRunDockerImage(){
    executor.buildDockerImage()
  }

  @Test
  fun testPackageCreation(){
    executor.executeJob()
  }
}