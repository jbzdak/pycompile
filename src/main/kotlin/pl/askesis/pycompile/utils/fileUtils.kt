
@file:JvmName("Utils")
@file:JvmMultifileClass

package pl.askesis.pycompile.utils

import org.apache.commons.io.IOUtils
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path
import java.security.MessageDigest
import java.util.*
import javax.xml.bind.DatatypeConverter

internal class ResourceClass;

class InvalidHashException(message: String): IOException(message);

fun calculateFileSha256(path: Path): ByteArray?{
  if (!Files.exists(path)){
    return null;
  }

  val digest = MessageDigest.getInstance("SHA-256");
  Files.newInputStream(path).use {
    val array = ByteArray(1024);
    var numread = it.read(array);
    while ( numread > 0 ){
      digest.update(array, 0, numread);
      numread = it.read(array);
    }
  }

  return digest.digest();
}

fun hexEncodedToByteArray(hexEncodedSha: String): ByteArray{
  return DatatypeConverter.parseHexBinary(hexEncodedSha);
}

fun calculateStringSha256(contents: String): ByteArray{
  val digest = MessageDigest.getInstance("SHA-256");
  digest.update(contents.toByteArray());
  return digest.digest();
}

fun copyStringToFile(contents: String, target: Path){
  Files.write(target, contents.toByteArray(Charset.forName("UTF-8")));
}

fun resourceToString(resourceName: String): String{
  return ResourceClass::class.java.getResourceAsStream(resourceName).use {
    IOUtils.toString(it, Charset.forName("UTF-8"));
  }
}

internal fun hasFileChanged(contents: String, target: Path): Boolean{
  val sha = calculateFileSha256(target);
  if (sha != null){
    val resourceSha = calculateStringSha256(contents)
    if (Arrays.equals(sha, resourceSha)){
      return false;
    }
  }
  return true;
}

internal fun hasFileChanged(expectedSha: ByteArray, target: Path): Boolean{
  val sha = calculateFileSha256(target);
  return Arrays.equals(sha, expectedSha);
}

fun copyStringToFileIfChanged(contents: String, target: Path){
  if (hasFileChanged(contents, target)){
    copyStringToFile(contents, target);
  }
}

fun downloadFile(url: URL, target: Path, sha256: ByteArray){
  FileOutputStream(target.toFile()).use { out ->
    url.openStream().use { input ->
      IOUtils.copy(input, out);
    }
  }
  val actualSha = calculateFileSha256(target);
  if (!Arrays.equals(sha256, actualSha)){
    throw InvalidHashException("Error verifying the file");
  }
}

fun downloadFileIfMissing(url: URL, target: Path, sha256: ByteArray){
  if (hasFileChanged(sha256, target)){
    return;
  }
  downloadFile(url, target, sha256);
}
