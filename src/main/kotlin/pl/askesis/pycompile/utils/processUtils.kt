
@file:JvmName("Utils")
@file:JvmMultifileClass

package pl.askesis.pycompile.utils

import org.apache.commons.io.IOUtils
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path
import java.security.MessageDigest
import java.util.*
import javax.xml.bind.DatatypeConverter

class ProcessError(msg: String): Exception(msg)

fun checkCall(command: Array<String>, cwd: Path? = null){
  val pb = ProcessBuilder(command.toList());
  if (cwd != null){
    pb.directory(cwd.toFile());
  }
  pb.redirectOutput(ProcessBuilder.Redirect.INHERIT)
  val process = pb.start()
  process.waitFor();
  if (process.exitValue() != 0){
    throw ProcessError("Outout of command ${command.joinToString(" ")} is ${process.exitValue()}")
  }
}