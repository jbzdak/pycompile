package pl.askesis.pycompile

import com.hubspot.jinjava.Jinjava
import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils
import pl.askesis.pycompile.config.ConfigParser
import pl.askesis.pycompile.config.ContextConfig
import pl.askesis.pycompile.config.ParsedConfig
import pl.askesis.pycompile.utils.checkCall
import pl.askesis.pycompile.utils.copyStringToFileIfChanged
import pl.askesis.pycompile.utils.downloadFileIfMissing
import pl.askesis.pycompile.utils.resourceToString
import java.io.StringWriter
import java.net.URI
import java.net.URL
import java.nio.file.FileAlreadyExistsException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*

enum class BaseSystem(
  shortName: String,
  dockerImage: String
){
  DEBIAN_JESSIE("jessie", "debian:jessie"),
  UbuntuXenial("xenial", "ubuntu:xenial");

  val dockerImage = dockerImage;
  val shortName = shortName;
}

class DebDependency(packageName: String?=null, packageVersion: String?=null){
  var packageName = packageName;
  var packageVersion = packageVersion;

  fun asDependency(): String{
    if (packageVersion == null){
      return packageName ?: throw IllegalStateException();
    }
    return "$packageName==$packageVersion"
  }
}

class JobContext(
  outputDir: Path,
  resourcesDir: Path,
  config: ParsedConfig
): AutoCloseable {

  val outputDir = outputDir
  val resourcesDir = resourcesDir
  val config = config
  var tempDir: Path = makeTempDir();

  init {
    Files.createDirectories(outputDir)
    Files.createDirectories(resourcesDir)
  }

  val pythonsDir: Path
    get() {
      val pythons = resourcesDir.resolve("python")
      Files.createDirectories(pythons);
      return pythons
    }


  constructor(config: ParsedConfig):
    this(
      outputDir=config.config.rootContextDir.resolve("output"),
      resourcesDir=config.config.rootContextDir.resolve("resources"),
      config = config
    )

  fun makeTempDir() : Path {
    return Files.createTempDirectory("compilepy");
  }

  override fun close(){
    FileUtils.deleteDirectory(tempDir.toFile());
  }

  internal fun downloadPythons(){
    for ((versionNo, pCfg) in config.pythons) {
      downloadFileIfMissing(
        URL(pCfg.downloadUrl),
        this.pythonsDir.resolve(pCfg.fileName),
        pCfg.sha
      )
    }
  }

  fun prepareContext(){
    downloadPythons()
  }

  companion object{
    fun mockContext(): JobContext{
      return JobContext(
        ParsedConfig(config = ContextConfig(
          Files.createTempDirectory("test-pycompule")
        ))
      )
    }
    fun testContext(): JobContext{
      return JobContext(
        ConfigParser.read(
          URI("classpath:///testConfig.yml")
        )!!
      )
    }
  }
}


internal fun renderTemplate(template: String, context: Map<String, Any>): String{
//  val engine = PebbleEngine.Builder().loader(StringLoader()).build();
//  val compiledTemplate = engine.getTemplate(template);
//
//  val writer = StringWriter();
//  compiledTemplate.evaluate(writer, context);
//
//  return writer.toString();
  val jinjava = Jinjava();

  return  jinjava.render(template, context);
}


class PythonCompileJobDefinition(
  baseSystem: BaseSystem? = null,
  pythonVersion: String? = null,
  architecture: String = "amd64",
  dependencies: List<DebDependency> = emptyList(),
  compileDependencies: List<DebDependency> = emptyList()
) {

  var baseSystem = baseSystem;
  var architecture = architecture;
  var pythonVersion = pythonVersion;
  var dependencies = dependencies;
  var compileDependencies = compileDependencies;
  var debian = DebianContext();

  val pythonDirRoot: String
    get() = "Python-$pythonVersion"

  internal val __architecture: String
    get() = architecture;

  inner class DebianContext{
    val packageName: String
      get() = "python-opt-$pythonVersion";
    val compileDeps: String
      get() {
        return compileDependencies.map {
          dep -> dep.asDependency() }.joinToString(" ");
      };
    val depends: String
      get() {
        return dependencies.map {
          dep -> dep.asDependency() }.joinToString(", ");
      }
    // TODO: Add build versioning, so we get 2.7.11-b123 :)
    val version: String
      get() = pythonVersion!!;
    val architecture: String
      get() = __architecture;
  }
}


abstract class JobExecutor(job: PythonCompileJobDefinition, context: JobContext){

  protected var job = job;
  protected var context = context;

  abstract fun executeJob();

  fun copyResourceToWorkdir(resourceName: String, workdirName: String){
    copyStringToFileIfChanged(
      resourceName,
      context.tempDir.resolve(workdirName)
    )
  }

  protected fun renderTemplate(templateName: String): String{

    val context = HashMap<String, Any>();
    context.put("job", job);

    val template = javaClass.getResourceAsStream(templateName).use {
      IOUtils.toString(it, "UTF-8");
    }

    return renderTemplate(template, context);
  }



}

abstract class PythonCompileJobExecutor(job: PythonCompileJobDefinition, context: JobContext): JobExecutor(job, context){

  private fun copyDebianTemplates(){
    val debianFolder = context.tempDir.resolve("DEBIAN");
    Files.createDirectories(debianFolder);
    val templatesPackage = "/pl/askesis/pycompile/docker/compileImg/debian/"
    copyStringToFileIfChanged(
      renderTemplate(templatesPackage + "control.j2"),
      debianFolder.resolve("control")
    )
    copyStringToFileIfChanged(
      renderTemplate(templatesPackage + "conffiles.j2"),
      debianFolder.resolve("conffiles")
    )
  }

  protected open fun prepareBuildDir(){
    copyDebianTemplates()
  }

}


class DockerExecutor(job: PythonCompileJobDefinition, context: JobContext): PythonCompileJobExecutor(job, context) {

  val dockerTag: String
    get() = "pycompile/${job.baseSystem!!.dockerImage}/${job.pythonVersion}".replace("/", "_")

  override fun executeJob() {
    buildDockerImage()
    copyPreparedPackage()
  }

  internal fun templateDockerFile(): String{
    return renderTemplate("/pl/askesis/pycompile/docker/compileImg/Dockerfile");
  }

  internal fun saveDockerFile(){
    val dockerfile = context.tempDir.resolve("Dockerfile")
    copyStringToFileIfChanged(templateDockerFile(), dockerfile)
  }

  internal fun copyPython(){
    val python = context.config.pythons.get(
      job.pythonVersion
    )!!
    val src = context.pythonsDir.resolve(
      python.fileName
    );
    val dest = context.tempDir.resolve(
      "python.tar.xz"
    )
    try {
      Files.copy(src, dest);
    } catch (e: FileAlreadyExistsException){

    }
  }

  override fun prepareBuildDir(){
    super.prepareBuildDir();
    saveDockerFile()
    copyPython()
    copyResourceToWorkdir(
      resourceToString(
        "/pl/askesis/pycompile/sources/${job.baseSystem!!.shortName}.sources.list"),
      "sources.list"
    )
  }

  internal fun buildDockerImage(){
    prepareBuildDir()
    val dockerCommand = arrayOf(
      "docker", "build",
      "-t", dockerTag,
      "."
    )
    checkCall(dockerCommand, cwd = context.tempDir)
  }

  internal fun copyPreparedPackage(){
    val dockerCommand = arrayOf(
      "docker", "run",
      "-v",  "${context.outputDir}:/tmp/run-output",
      dockerTag,
      "cp", "-rav", "/tmp/finished-package/", "/tmp/run-output"
    )
    checkCall(dockerCommand, cwd = context.tempDir)
  }
}