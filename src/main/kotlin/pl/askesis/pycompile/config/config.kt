package pl.askesis.pycompile.config

import org.yaml.snakeyaml.TypeDescription
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.Constructor
import pl.askesis.pycompile.DebDependency
import pl.askesis.pycompile.PythonCompileJobDefinition
import pl.askesis.pycompile.utils.hexEncodedToByteArray
import java.io.InputStream
import java.net.URI
import java.net.URL
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*

class YamlParsedConfig(){

  @JvmField var jobs: List<PythonCompileJobDefinition> = ArrayList();
  @JvmField var pythons: Map<String, PythonVersionYamlBean> = HashMap();
  @JvmField var config: ContextConfigYamlBean? = null;

  fun prepareConfig(): ParsedConfig{
    val newPythons = HashMap<String, PythonVersion>()
    for ((k, v) in pythons){
      newPythons.put(k, PythonVersion(v));
    }

    return ParsedConfig(
      jobs = jobs,
      pythons = newPythons,
      config = ContextConfig(
        rootContextDir = Paths.get(config!!.rootContextDir!!)
      )
    )
  }
}

data class ParsedConfig(
  val jobs: List<PythonCompileJobDefinition> = ArrayList(),
  val pythons: Map<String, PythonVersion> = HashMap(),
  val config: ContextConfig
)

class PythonVersion(
  downloadUrl: String,
  hexEncodedSha: String
){

  var downloadUrl = downloadUrl;
  var hexEncodedSha = hexEncodedSha;

  constructor(bean: PythonVersionYamlBean): this(bean.downloadUrl!!, bean.hexEncodedSha!!){};

  val fileName: String
    get() = computeFileName()

  val sha: ByteArray
    get() = hexEncodedToByteArray(hexEncodedSha)

  private fun computeFileName(): String{
    val url = URL(downloadUrl);
    val part = url.file.split("/");
    if (part.size == 0){
      throw RuntimeException();
    }
    return part[part.size-1];
  }
}

data class PythonVersionYamlBean(
  var downloadUrl: String? = null,
  var hexEncodedSha: String? = null
)


data class ContextConfigYamlBean(
  var rootContextDir: String? = null
)

data class ContextConfig(
  val rootContextDir: Path
)


class ConfigParser(){

  companion object {

    val CONFIG_URL_ENVVAR = "PYCOMPILE_CONFIG";

    fun makeParser(): Yaml {
      val configDesc = TypeDescription(YamlParsedConfig::class.java)
      configDesc.putListPropertyType("jobs", PythonCompileJobDefinition::class.java)
      configDesc.putMapPropertyType(
        "pythons",
        String::class.java,
        PythonVersionYamlBean::class.java
      );
      val jobDesc = TypeDescription(PythonCompileJobDefinition::class.java)

      jobDesc.putListPropertyType("dependencies", DebDependency::class.java.javaClass)
      val constructor = Constructor(configDesc);
      constructor.addTypeDescription(jobDesc);

      return Yaml(constructor);
    }

    fun read(stream: InputStream): ParsedConfig {
      val parser = makeParser();
      stream.buffered().reader().use {
        return (parser.load(it) as YamlParsedConfig).prepareConfig();
      }
    }

    fun read(uri: URI?): ParsedConfig? {
      if (uri == null) {
        return null;
      }
      if (uri.scheme == "classpath") {
        return ConfigParser::class.java.getResourceAsStream(uri.path).use {
          read(it)
        }
      }
      return uri.toURL().openStream().use {
        read(it)
      }
    }

    fun loadDefaultConfig(): ParsedConfig? {
      return read(URI(System.getenv().getOrElse(
        CONFIG_URL_ENVVAR, { "classpath:///pl/askesis/pycompile/docker/compileImg/config.yml" }
      )))
    }
  }
}