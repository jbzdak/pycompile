Readme
======

Scripts that created deb files that install python versions
to `/opt/python/<version>` (e.g.: `/opt/python/2.7.11`).

When your Python application contains numpy/scipy provisinoning new server
starts to take longer and longer.

## TODO

1. Add a way to launch python compilation process from command line.
2. Create a repository with pythons compiled using this scripts.
3. Add packages that install numpy/scipy and firends there.

